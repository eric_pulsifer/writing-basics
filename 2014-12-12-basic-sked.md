## Sked, kinda sorta

**0730** Up

**0830 or 0900** Start freelance work:
   - Pitching
   - Research
   - Interviews
   - Writing, freelance-related
   - Netbook or desktop, depending on task
   - Do this until 1200 or 1300
   
**1300** Whenever Dad is functional
   - The caregiving thing
   - Any of my real work when I can.
   


**1800** Start project work
   - Fiction writing
   - Shut down writing at 2200.
   
**2200** Dinner
   - Recreational reading until bedtime.

**2400** Bedtime. No later than 0030.






Enter text in [Markdown](http://daringfireball.net/projects/markdown/). Use the toolbar above, or click the **?** button for formatting help.
